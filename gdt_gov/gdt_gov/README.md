 - Để file chứa các mã số thuế trong thư mục này.
 - Từ thư mục này (/crawl_thue/gdt_gov/gdt_gov/) chạy scrapy bằng lệnh sau:

                scrapy crawl nopthue -a file_code={tên file chứa mã số thuế}

 - output sẽ gồm 3 file:
    + "info.csv": file chứa thông tin lấy về từ các mã số thuế đúng.
    + "error_tax_code.txt": file chứa các mã số thuế sai (sai mã, sai định dạng).
    + "except_tax_code.txt": file chứa các mã số ngoại lệ(lỗi đặc biệt nào đó).
       Hiện tại chưa tìm thấy mã nào thế này. Em xét trường hợp đặc biệt này
       để đảm bảo chương trình luôn chạy.