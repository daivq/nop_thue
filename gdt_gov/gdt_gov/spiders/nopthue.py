#!usr/bin/env python
# -*- coding: utf-8 -*-

import time
import csv
from datetime import datetime

import scrapy
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, \
    UnexpectedAlertPresentException


class NopthueSpider(scrapy.Spider):
    name = "nopthue"
    allowed_domains = ["https://nopthue.gdt.gov.vn"]
    start_urls = [
        "https://nopthue.gdt.gov.vn/epay_nnt/home.jsp"
    ]

    def __init__(self, file_code=''):
        self.driver = webdriver.Firefox()
        self.file_code = file_code

    def parse(self, response):
        self.driver.get(response.url)
        tax_code_file = open(self.file_code)
        _date = str(datetime.now().date())
        csvfile = open('info-%s.csv' % _date, 'w')
        error_tax_code = open('error_tax_code_%s.txt' % _date, 'w')
        except_tax_code = open('except_tax_code_%s.txt' % _date, 'w')
        fieldnames = ['code', 'node', 'phone', 'email', 'serial', 'issuer', 'acc_name', 'email_kthue', 'tenNLH', 'from_date', 'to_date']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames,
                                quoting=csv.QUOTE_NONNUMERIC, dialect=csv.excel)
        writer.writeheader()
        for ms in tax_code_file:
            mst = str(ms).strip()
            try:
                register = self.driver.find_element_by_xpath('//img[@alt="register"]')
                register.click()
                time.sleep(2)
                tax_code = self.driver.find_element_by_xpath('//input['
                                                             '@class="input_common"]')
                tax_code.clear()
                tax_code.send_keys(ms)

                try:
                    self.driver.find_element_by_id("tel")
                    self.driver.find_element_by_id("serial")
                except NoSuchElementException:
                    show_info = self.driver.find_element_by_xpath('//input[@class="btn_type1"]')
                    show_info.click()
                    time.sleep(1)

                try:
                    self.driver.find_element_by_xpath('//div[@style="font-size: 12px ;margin-left:50px;font-style:italic;"]')
                except NoSuchElementException:
                    pass
                except UnexpectedAlertPresentException:
                    pass
                try:
                    self.driver.find_element_by_id("tel")
                except NoSuchElementException:
                    error_tax_code.write(ms)
                    continue
                try:
                    node_tag = self.driver.find_element_by_xpath('//div[@style="font-size: 12px ;margin-left:50px;font-style:italic;"]')
                    node = node_tag.text
                except NoSuchElementException:
                    node = ""
                phone_tag = self.driver.find_element_by_id("tel")
                phone = phone_tag.get_attribute("value").strip()

                email_tag = self.driver.find_element_by_name("email")
                email = email_tag.get_attribute("value")

                serial_tag = self.driver.find_element_by_id("serial")
                serial = serial_tag.get_attribute("value")

                issuer_tag = self.driver.find_element_by_id("issuer")
                issuer = issuer_tag.get_attribute("value")

                acc_name_tag = self.driver.find_element_by_id("ten_tk")
                acc_name = acc_name_tag.get_attribute("value")

                email_kthue_tag = self.driver.find_element_by_name("email_kthue")
                email_kthue = email_kthue_tag.get_attribute("value")

                tenNLH_tag = self.driver.find_element_by_id("tenNLH")
                tenNLH = tenNLH_tag.get_attribute("value")

                from_date_tag = self.driver.find_element_by_name("tuNgay")
                from_date = from_date_tag.get_attribute("value")

                to_date_tag = self.driver.find_element_by_name('denNgay')
                to_date = to_date_tag.get_attribute("value")

                writer.writerow({'code': mst,
                                 'node': node.encode('utf-8'),
                                 'phone': phone.encode('utf-8'),
                                 'email': email.encode('utf-8'),
                                 'serial': serial.encode('utf-8'),
                                 'issuer': issuer.encode('utf-8'),
                                 'acc_name': acc_name.encode('utf-8'),
                                 'email_kthue': email_kthue.encode('utf-8'),
                                 'tenNLH': tenNLH.encode('utf-8'),
                                 'from_date': from_date.encode('utf-8'),
                                 'to_date': to_date.encode('utf-8')})
            except Exception, e:
                except_tax_code.write(ms)
                continue
        tax_code_file.close()
        csvfile.close()
        error_tax_code.close()
        self.driver.quit()
